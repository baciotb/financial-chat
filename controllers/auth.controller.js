const router = require('express').Router();
const userService = require('../services/users.service');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

/**
 * Login and check if user exists
 */
router.post('/login', async (req, res) => {
    try {
        const {username, password} = req.body;
        const encoded = crypto.createHash('md5').update(password).digest('hex');
        const foundUser = await userService.getByQuery({username, password: encoded})
        if(foundUser) {
            const token = jwt.sign(foundUser.toObject(), process.env.JWT_SECRET);
            res.json({token});
        } else {
            res.sendStatus(401);
        }
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
})

module.exports = router;