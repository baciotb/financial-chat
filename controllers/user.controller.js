const router = require('express').Router();
const userService = require('../services/users.service');

/**
 * Register new users
 */
router.post('/register', async (req, res) => {
    try {
        const {username, password} = req.body;
        await userService.create({username, password})
        res.status = 200;
        res.send({status: 200});
    } catch(err) {
        console.error(err);
        res.sendStatus(500);
    }
})

module.exports = router;