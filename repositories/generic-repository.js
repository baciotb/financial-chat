/**
 * Obtains many items from Model and filters by params
 * @param {Model} Model 
 * @param {Object} params 
 */
const fetch = async (Model, params) => {
    return new Promise((res, rej) => {
        Model.find(params).then((err, items) => {
            err && rej(err);
            res(items);
        })
    });
}

/**
 * Obtains one item from Model by filtering params
 * @param {Model} Model 
 * @param {Object} params 
 */
const find = async (Model, params) => {
    return new Promise((res, rej) => {
        Model.findOne(params).then((item, err) => {
            if(err) return rej(err);
            res(item);
        })
    });
}

/**
 * Saves object document to Model
 * @param {Model} Model 
 * @param {Object} obj that fits Model Schema
 */
const save = async (Model, obj) => {
    return new Promise((res, rej) => {
        Model.create(obj, err => {
            if(err) return rej(err);
            res();
        })
    });
}

module.exports = {
    fetch, save, find
}