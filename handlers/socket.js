const broker = require('../broker');
const jwt = require('jsonwebtoken');
const { isCommand } = require('../helpers/broker');
const { create: createPost } = require('../services/posts.service');
let io;

/**
 * Set socket listeners and register callbacks
 * @param {socket.io} io 
 */
const init = io => () => {
    // Verify authentication
    io.use(function (socket, next) {
        if (socket.handshake.query && socket.handshake.query.token) {
            jwt.verify(socket.handshake.query.token, process.env.JWT_SECRET, function (err, decoded) {
                if (err) return next(new Error('Authentication error'));
                socket.decoded = decoded;
                next();
            });
        } else {
            next(new Error('Authentication error'));
        }
    }).on('connection', socket => {
        console.log('User connected');
        socket.on('disconnect', function () {
            console.log('user disconnected');
        });

        socket.on('chat message', function (msg) {
            if (isCommand(msg.msg)) {
                const { STOCK } = broker.queues.queues;
                const [, stock] = msg.msg.split("=");
                broker.publisher.publish(STOCK, { ...msg, stock });
            } else {
                createPost(msg);
            }
            io.emit('chat message', msg);
        });
    })
}

module.exports = (http) => {
    if (!io) {
        io = require('socket.io')(http);
    }
    return {
        io,
        init: init(io)
    }
};