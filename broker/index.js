module.exports = {
    publisher: require('./publisher'),
    consumer: require('./consumer'),
    queues: require('./queues')
}