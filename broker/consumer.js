var amqp = require('amqplib/callback_api');
const queues = require('./queues').consumers;

// Init consumers on channel and inject dependencies
module.exports.init = dependencies => {
    amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        queues.forEach(q => {
            channel.assertQueue(q.name, {
                durable: false
            });
            channel.consume(q.name, q.consumer(channel, dependencies), {
                noAck: true
            });
            console.log('%s QUEUE LISTENING', q.name);
        })
    });
});
}