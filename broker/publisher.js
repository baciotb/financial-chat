var amqp = require('amqplib/callback_api');
const { sendToQueue } = require('../helpers/broker');

// Publish to queue by creating channel and closing afterwards
module.exports.publish = (queueName, msg) => amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        console.error(error1);
        if (error1) {
            throw error1;
        }

        sendToQueue(channel, queueName, Buffer.from(JSON.stringify(msg)));

        console.log(" [x] Sent %s", msg);
    });
    setTimeout(function() {
        connection.close();
    }, 500);
});
