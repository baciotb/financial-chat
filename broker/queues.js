var axios = require('axios').default;
var csv = require('csvtojson');
const { sendToQueue } = require('../helpers/broker');

const QUEUES = {
    STOCK: 'STOCK',
    STOCK_REPLY: 'STOCK_REPLY'
}

// Define queues and queue consumers
const consumers = [
    {
        name: QUEUES.STOCK,
        // Define consumer for stock request
        consumer: channel => msg => {
            console.log(" [x] Received %s on %s", msg.content.toString(), QUEUES.STOCK);
            const {stock, from} = JSON.parse(msg.content.toString());
            axios.get(`https://stooq.com/q/l/?s=${stock}&f=sd2t2ohlcv&h&e=csv`).then(response => {
                csv().fromString(response.data).then(([parsed]) => {
                    sendToQueue(channel, QUEUES.STOCK_REPLY, Buffer.from(JSON.stringify({...parsed, from})));
                });
            }, console.error);
        }
    },
    {
        name: QUEUES.STOCK_REPLY,
        // define consumer for stock OUTPUT
        consumer: (channel, [io]) => msg => {
            console.log(" [x] Received %s on %s", msg.content.toString(), QUEUES.STOCK_REPLY);
            const message = JSON.parse(msg.content.toString());
            io.emit('chat message', {from: 'financial-bot', msg: `@${message.from} ${message.Symbol} quote is $${message.Close} per share`});
        }
    }

]

module.exports = {
    consumers,
    queues: QUEUES
}

