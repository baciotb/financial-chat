require('dotenv').config();
const broker = require('./broker');
var app = require('express')();
var http = require('http').createServer(app);
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var socket = require('./handlers/socket')(http);
// General registration
app.use(bodyParser.json());
app.use(cors());

// Declare controllers
const authController = require('./controllers/auth.controller');
app.use('/api/auth', authController);
const userController = require('./controllers/user.controller');
app.use('/api/users', userController);

async function main() {
    try {
        // Init socket
        socket.init();
        // Init Consumers and provide dependencies
        broker.consumer.init([socket.io]);
        // TODO: Move to .env
        await mongoose.connect(process.env.MONGO_URL);
        await new Promise(res => {
            http.listen(3000, function(){
                res('listening on *:3000');
            });
        })
    } catch(err) {
        throw err;
    }
}
main().then(console.log);