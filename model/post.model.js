const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    from: String,
    msg: String,
    timestamp: Number
})

module.exports = mongoose.model('Post', postSchema);