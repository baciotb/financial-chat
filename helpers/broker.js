/**
 * Asserts channel and sends message
 * @param {Channel} channel to send msg through
 * @param {String} queueName to send msg to
 * @param {any} msg to be sent 
 */
const sendToQueue= (channel, queueName, msg) => {
    channel.assertQueue(queueName, {
        durable: false
    })
    channel.sendToQueue(queueName, msg);
}

/**
 * Checks is message is command of form /command=valueOfCommand
 * @param {String} msg 
 */
const isCommand = msg => /^\/[a-z]+=\S+$/.test(msg.trim());



module.exports = {sendToQueue, isCommand}