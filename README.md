# FinancialChat

This is the backend for a financial chat service.

## Requirements
node v11 or greater is required

## Getting started
Run `npm i` to install dependencies

## Development server
Create a `.env` file following the example of `.env.example` (for the purpose of this test, these are the correct values, but you can change them as you wish)
Run `docker-compose up -d` to start broker services and MongoDB.
Then, run `npm run start` to get server started. Server will listen on `http://localhost:3000/`.
