const Post = require('../model/post.model');
const repository = require('../repositories/generic-repository');
/**
 * Create a new Post
 * @param {T implements Post} postLikeObject 
 */
const create = async postLikeObject => {
    return await repository.save(Post, postLikeObject);
}

module.exports = {
    create,
}