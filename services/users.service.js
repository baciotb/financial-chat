const User = require('../model/user.model');
const crypto = require('crypto');
const repository = require('../repositories/generic-repository');
/**
 * Creates a User
 * @param {T implements User} userLikeObject 
 */
const create = async userLikeObject => {
    const { password } = userLikeObject;
    const user = {
        ...userLikeObject,
        password: crypto.createHash('md5').update(password).digest('hex')
    }
    return await repository.save(User, user);
}

/**
 * Obtains first user by params
 * @param {Partial<User>} userQuery 
 */
const getByQuery = async userQuery => {
    return await repository.find(User, userQuery);
}

module.exports = {
    create,
    getByQuery
}